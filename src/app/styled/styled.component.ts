import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-styled',
  templateUrl: './styled.component.html',
  styleUrls: ['./styled.component.css']
})
export class StyledComponent implements OnInit {

  css_class: string;
  css_classes: string[];
  css_object = {};
  constructor() {
    this.css_classes =[];
  }

  ngOnInit() {
  }

  addClass() {
    this.css_classes.push(this.css_class);
    this.css_object[this.css_class] = true;
    this.css_class = '';
  }

}
