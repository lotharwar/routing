import { Injectable } from '@angular/core';
import { Article } from './article';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  articles: Article[];
  constructor() { 
    this.articles = JSON.parse(localStorage.getItem('articles') || '[]');
  }

  findById(id) {
    return this.articles.find(x => x.id == id);
  }

  deleteById(id) {
    let article = this.findById(id);
    const index = this.articles.indexOf(article);
    if (index > -1) {
      this.articles.splice(index, 1);
    }
    localStorage.setItem('articles', JSON.stringify(this.articles));
  }
}
