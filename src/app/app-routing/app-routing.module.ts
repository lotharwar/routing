import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { MainComponent } from '../main/main.component';
import { ArticleComponent } from '../article/article.component';
import { ErrorComponent } from '../error/error.component';
import { StyledComponent } from '../styled/styled.component';
import { TermometroComponent } from '../termometro/termometro.component';

import { WachiGuard } from '../wachi.guard';

const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'article/:id', component: ArticleComponent, canActivate: [WachiGuard] },
  { path: 'error', component: ErrorComponent },
  { path: 'styled', component: StyledComponent },
  { path: 'termometro', component: TermometroComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
