import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-termometro',
  templateUrl: './termometro.component.html',
  styleUrls: ['./termometro.component.css']
})
export class TermometroComponent implements OnInit {
  temperature: number;
  constructor() {
    this.temperature =20;
  }

  ngOnInit() {
  }

}
