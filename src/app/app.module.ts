import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { ArticleComponent } from './article/article.component';

import { ArticleService } from './article.service';
import { ArticleRowComponent } from './article-row/article-row.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { ErrorComponent } from './error/error.component';
import { StyledComponent } from './styled/styled.component';
import { TermometroComponent } from './termometro/termometro.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    ArticleComponent,
    ArticleRowComponent,
    ConfirmationComponent,
    ErrorComponent,
    StyledComponent,
    TermometroComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [ArticleService],
  bootstrap: [AppComponent]
})
export class AppModule { }
